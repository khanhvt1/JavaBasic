package selenium.demo;
/**
 * author:FWA.CP.
 * Created date: Jul 7, 2014 
 * Version: 1.0
 */
public class MyFirstJavaProgram {
	/*
	 * This is my first java program. This will print 'Hello World' as the
	 * output
	 */

	public MyFirstJavaProgram(){
	 // This is Constructor	
	}
	
	public MyFirstJavaProgram(String strName){
		System.out.println(" This name is: " + strName);
		}
	public static void main(String[] args) {
	
		MyFirstJavaProgram myFirstJavaProgram = new MyFirstJavaProgram();
		
		MyFirstJavaProgram mProgram = new MyFirstJavaProgram("MyFirstJava");
		System.out.println("Hello World!");
	}
}

