/**
 *Project name: JavaSelenium
 *Package name: selenium.demo.p1
 */
package selenium.demo.p1;
/**
 * author:ThuanVD3. created date: Oct 25, 2014 version: 1.0
 */
public class ClassA {

	public void publicSumXY(int x, int y) {
	}

	protected void protectedSumXYZ(int x, int y, int z) {
	}
	
	private void privateSumAB(int a, int b){
	}
	
	void defaultSumABC(int x, int y, int z){
		
	}
	
	public static void main(String[] args) {
		ClassA classA = new ClassA();
		
	} 
}
