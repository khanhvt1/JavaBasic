/**
*Project name: JavaSelenium
*Package name: selenium.demo.p1
*/
package selenium.demo.p1;
/**
 *author:ThuanVD3.
 *created date: Oct 25, 2014
 *version: 1.0
 */
public class ClassE extends ClassA {

	
	public static void main(String[] args) {
		ClassE  classE = new ClassE();
		classE.protectedSumXYZ(1, 2, 3);
		classE.publicSumXY(1,2);
		classE.defaultSumABC(1, 3, 4);
		
	}
}

