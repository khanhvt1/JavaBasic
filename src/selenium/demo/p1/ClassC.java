/**
 *Project name: JavaSelenium
 *Package name: selenium.demo.p1
 */
package selenium.demo.p1;
/**
 * author:ThuanVD3. created date: Oct 25, 2014 version: 1.0
 */
public class ClassC {
	public static void main(String[] args) {
		ClassA classA = new ClassA();
		
		classA.publicSumXY(1, 2);
		classA.protectedSumXYZ(1, 2, 3);
		classA.defaultSumABC(1, 3, 4);
		
	}
}
