package selenium.demo;
import java.io.IOException;
import java.util.Scanner;

/**
 * author:FWA.CTC. created date: Jul 9, 2014 version: 1.0
 */
public class InputKeyBoard {
	public static void main(String[] args) throws IOException {
		System.out.print("Input number 1: ");
		Scanner scanner = new Scanner(System.in);
		int num1 = scanner.nextInt();
		
		System.out.print("Input number 2: ");
		scanner = new Scanner(System.in);
		int num2 = Integer.parseInt(scanner.nextLine());
		
		SubClass sb = new SubClass();
		
		System.out.println("sum num1 + num2 : " 
				+ sb.sumXY(num1, num2));
	}

}
