/**
 *Project name: JavaSelenium
 *Package name: selenium.demo
 */
package selenium.demo;
import java.util.ArrayList;
import java.util.List;

/**
 * author:FWA.CTC. 
 * created date: Jul 9, 2014 
 * version: 1.0
 */
public class ArrayListWithObject {
	public static void main(String[] args) {
		
		List arrayList = new ArrayList();
		
		Person person = new Person();
		person.setName("John");
		arrayList.add(person);
		
		person = new Person();
		person.setName("Petter");
		arrayList.add(person);
		
		System.out.println("Value at the first index: " + ((Person)arrayList.get(0)).getName());// Brot kaufen
	
		// Use for loop to look up arrayList
		int arrSize = arrayList.size();
		for (int i =0; i < arrSize; i++){
			System.out.println(" i: " + ((Person)arrayList.get(i)).getName());
		}
		
		arrayList.remove(0);
		
	}
}
