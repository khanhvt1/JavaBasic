package selenium.demo;
/**
 * author:FWA.CP. 
 * created date: Jul 7, 2014 
 * version: 1.0
 */
public class Person {
	private String name = "Lab4";
	private double height =12;
	private double weight;
	private int age;
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHealth() {
		return health;
	}

	public void setHealth(String health) {
		this.health = health;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}
	private String health;
	private String degree;
	private String experience;

	public void goBy() {
	}

	public void eatBy() {
	}

	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	
	public static void main(String[] args) {
		
		Person persion1 = new Person();
		persion1.setName("Selenium");
		
		Person persion2 = new Person();
		persion2.setName("Java");
		
		System.out.println(persion1.getName());
		
	}
}
