/**
 *Project name: JavaSelenium
 *Package name: selenium.demo
 */
package selenium.demo;
import java.util.ArrayList;
import java.util.List;

/**
 * author:FWA.CTC. 
 * created date: Jul 9, 2014 
 * version: 1.0
 */
public class ArrayListDemo {
	public static void main(String[] args) {
		
		List arrayList = new ArrayList();
		
		arrayList.add("Brot kaufen");
		arrayList.add("Handy auflanen");
		arrayList.add("11:30 John treffen");
		
		System.out.println("Value at the first index: " + arrayList.get(0));// Brot kaufen
	
		// Use for loop to look up arrayList
		int arrSize = arrayList.size();
		for (int i =0; i < arrSize; i++){
			System.out.println(" i: " + arrayList.get(i));
		}
		
		arrayList.remove("Brot kaufen"); 
		
		arrayList.remove(0);
		
	}
}
