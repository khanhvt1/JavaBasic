package selenium.demo;
public class PersonalContact {

	private String addressLookup;
	private String _postCode;
	private double _$currency;
	
	private int phone = 912345678;

	public String lookUpPerson() {
		return this.addressLookup;
	}

	protected String lookUpPostCode(String address) {
		return this._postCode;
	}

	double lookUpCurrency() {
		return this._$currency;
	}
	public static void main(String[] args) {
		PersonalContact personalContact = new PersonalContact();
		personalContact.lookUpCurrency();
	}
}
