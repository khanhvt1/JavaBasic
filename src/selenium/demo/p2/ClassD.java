/**
*Project name: JavaSelenium
*Package name: selenium.demo.p2
*/
package selenium.demo.p2;

import selenium.demo.p1.ClassA;

/**
 *author:ThuanVD3.
 *created date: Oct 25, 2014
 *version: 1.0
 */
public class ClassD extends ClassA {
		
	public void testProtected(){
		
		
	}
	
	public static void main(String[] args) {
		ClassD classD = new ClassD();
		classD.protectedSumXYZ(1, 2, 3);
		classD.publicSumXY(1, 2);
	}
}

