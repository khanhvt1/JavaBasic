/**
*Project name: JavaSelenium
*Package name: selenium.demo.p2
*/
package selenium.demo.p2;

import selenium.demo.p1.ClassA;

/**
 *author:ThuanVD3.
 *created date: Oct 25, 2014
 *version: 1.0
 */
public class ClassB {

	public static void main(String[] args) {
		ClassA classA = new ClassA();
		
		classA.publicSumXY(1,2);
	}
}

