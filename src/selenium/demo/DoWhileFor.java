package selenium.demo;
public class DoWhileFor {

	public static void main(String[] args) {
		int i = 10;
		int j = 10;
		int k = 10;

		// WHILE LOOP
		while (i < 12) {
			System.out.print(" " + i); // 10,11
			i++;
		}


		// DO LOOP
		do {
			j++;
			System.out.print("" + j); // 11,12
		} while (j < 12);

		// FOR LOOP

		for (i = 0; i < k; i++) {
			System.out.print(" " + i); // 0,1 2 3... 9
		}
	}
}
