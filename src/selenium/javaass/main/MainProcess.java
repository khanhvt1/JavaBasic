/**
*author:KhanhVT1.
*created date: Jun 19, 2018
*version: 1.0
*/
package selenium.javaass.main;

import java.util.Scanner;

import selenium.javaass.function.Division;

public class MainProcess {
	
		@SuppressWarnings("resource")
		public static void main(String[] args) {
//input from keyboard
			double A, B, C, result;
			Scanner scanner = null;
			System.out.print("Input number A: ");
			scanner = new Scanner(System.in);
			A = scanner.nextDouble();

			System.out.print("Input number B: ");
			scanner = new Scanner(System.in);
			B = scanner.nextDouble();

			System.out.print("Input number C: ");
			scanner = new Scanner(System.in);
		    C = scanner.nextDouble();
		    
		    Division a = new Division();
		    result = a.doDivision(A, B, C);
		    System.out.println("Result: " + result);
		    
	}
}